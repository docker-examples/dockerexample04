**Quellcode**
                                                                                                                                                
Zu finden im counter-Ordner. 

Besteht im Prinzip aus einer Main-Klasse welche eine Zahl aus/data/counter.txt ausliest (initial nicht vorhanden).
Anschliessend wird, ausgehend von der Zahl aus der Datei (oder 0 falls die Datei nicht vorhanden war), diese Zahl im Sekundentakt
um 1 erhoeht. Nach jeder Erhoehung wird die Zahl in die Datei geschrieben.

Beispiel dient dazu zu zeigen, wie man Daten ueber die Containerlaufzeit hinweg persistieren kann. Dadurch, dass das data-Verzeichnis
in den Container gemounted wird, kann der Container waehrend seiner Laufzeit auf den Inhalt zugreifen und ihn modifizieren. Wird
der Container beendet bleibt das Verzeichnis / die Datei natuerlich bestehen. Beim naechsten Container-Start wird die Datei dann ausgelesen.

**Docker**

Zu finden im docker-Ordner.

**Befehle**

Image bauen: `docker build -t dockerdemo/docker04:latest .` (im docker-Ordner)

Container starten: `docker run --rm dockerdemo/docker02` --> Wir sind im Container - nach Ausgabe von "Hello World!" ist der Container weg.

ODER

Container starten (Alternative): `docker run --rm -d dockerdemo/docker02` --> Container laeuft im Hintergrund und ist trotzdem direkt wieder weg.

Container beenden: `docker stop <container hash>` ODER `docker rm -f <container hash>`
**Quellcode**
                                                                                                                                                
Zu finden im helloworld-Ordner. 

Besteht im Prinzip aus einer Main-Klasse welche ein mal "Hello World!" ausgibt.

Beispiel dient dazu zu zeigen, dass der Container nur so lange läuft, bis der Hauptprozess beendet ist (was nach Ausgabe des "Hello World!" der Fall ist).

**Docker**

Zu finden im docker-Ordner.

**Befehle**

Image bauen: `docker build -t dockerdemo/docker02:latest .` (im docker-Ordner)

**Version: Bind Mount**
Container starten (mit Bind Mount): `docker run --rm -d --mount type=bind,source=/absoluter/Pfad/zu/docker-04/docker/data,target=/data dockerdemo/docker04:latest` --> data-Verzeichnis wird im Container unter /data gemounted

Container-Ausgaben beobachten: `docker logs -f <container hash>` --> Wir sehen, beim ersten Start _"Starte mit i = 0"_

Container beenden: `docker stop <container hash>` ODER `docker rm -f <container hash>`

Container erneut starten (mit Bind Mount): `docker run --rm -d --mount type=bind,source=/absoluter/Pfad/zu/docker-04/docker/data,target=/data dockerdemo/docker04:latest`

Container-Ausgaben beobachten: `docker logs -f <container hash>` --> Wir sehen, beim zweiten Start _"Starte mit i = 40"_ (oder wo auch immer der Counter stand als der Container zum ersten Mal beendet wurde)

Container beenden: `docker stop <container hash>` ODER `docker rm -f <container hash>`

ODER

**Version: Named Volume**

Named Volume erstellen: `docker volume create data-volume`

Container starten (mit Named Volume): `docker run --rm -d --mount source=data-volume,target=/data dockerdemo/docker04:latest` --> Verzeichnis welches sich hinter data-volume verbirgt (wird von Docker verwaltet) wird im Container unter /data gemounted

_Info: Pfad welcher sich hinter date-volume verbirgt kann gefunden werden via `docker volume inspect data-volume`_

Container-Ausgaben beobachten: `docker logs -f <container hash>` --> Wir sehen, beim ersten Start _"Starte mit i = 0"_

Container beenden: `docker stop <container hash>` ODER `docker rm -f <container hash>`

Container erneut starten (mit Bind Mount): `docker run --rm -d --mount type=bind,source=/absoluter/Pfad/zu/docker-04/docker/data,target=/data dockerdemo/docker04:latest`

Container-Ausgaben beobachten: `docker logs -f <container hash>` --> Wir sehen, beim zweiten Start _"Starte mit i = 40"_ (oder wo auch immer der Counter stand als der Container zum ersten Mal beendet wurde)

Container beenden: `docker stop <container hash>` ODER `docker rm -f <container hash>`