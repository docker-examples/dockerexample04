import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    public static final String DATEI_PFAD = "/data/counter.txt";

    public static void main(String[] args) throws InterruptedException {

        int i = leseErsteZeileAusDatei(DATEI_PFAD);
        System.out.println("Starte mit i = " + i);
        while (true) {
            ++i;
            schreibeAktuellenCounterInDatei(i, DATEI_PFAD);
            System.out.println(i);
            Thread.sleep(1000);
        }
    }

    private static int leseErsteZeileAusDatei(String dateiPfad) {
        try {
            String counter = Files.readAllLines(Paths.get(dateiPfad)).get(0);
            return Integer.valueOf(counter);
        } catch (IOException e) {
            // Datei nicht vorhanden, dann starten wir mit i = 0
            return 0;
        }
    }

    private static void schreibeAktuellenCounterInDatei(Integer counter, String dateiPfad) {
        try {
            Files.write(Paths.get(dateiPfad), counter.toString().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
